# ~/.zshrc

export EDITOR=vim
set -o emacs

set -U path
path=( ~/.local/bin ~/code/bin ~/.npm/bin $path )
export PATH

export ZPLUG_HOME=~/.zplug

if [ ! -d "$ZPLUG_HOME" ]; then
	echo "zplug not installed... cloning..."
	git clone https://github.com/zplug/zplug "$ZPLUG_HOME"
fi

#source /usr/share/zsh/scripts/zplug/init.zsh
source "$ZPLUG_HOME/init.zsh"

zplug 'zplug/zplug', hook-build:'zplug --self-manage'

# theme
zplug mafredri/zsh-async, from:github
zplug sindresorhus/pure, use:pure.zsh, from:github, as:theme

# core zsh stuff
zplug "zsh-users/zsh-completions", defer:0
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "zsh-users/zsh-history-substring-search", defer:3

# zsh extras
zstyle ':notify:*' command-complete-timeout 1
zplug "marzocchi/zsh-notify"
zplug "Tarrasch/zsh-autoenv"

# enhancd
# ENHANCD_DOT_SHOW_FULLPATH=1
# # ENHANCD_DISABLE_HOME=1
# ENHANCD_COMMAND=c
# zplug "b4b4r07/enhancd", use:init.sh

# z
_Z_DATA=~/.cache/z
zplug "rupa/z", use:z.sh # fancy cd replacement

if ! which fzf >/dev/null; then
	echo "install fzf for enhancd"
fi

# utils
zplug "supercrabtree/k" # fancy ls replacement
zplug "pixelb/ps_mem", as:"command", use:"ps_mem.py", rename-to:"ps_mem"
zplug "mhagger/git-test", as:"command", use:"bin/git-test"

# package 'command-not-found' on ubuntu
if [[ -s '/etc/zsh_command_not_found' ]]; then
  source '/etc/zsh_command_not_found'
fi

# package 'aur/command-not-found' on arch linux
if [[ -s '/etc/profile.d/cnf.sh' ]]; then
  source '/etc/profile.d/cnf.sh'
fi

PURE_CMD_MAX_EXEC_TIME=9223372036854775807
REPORTTIME=1
REPORTMEMORY=100
TIMEFMT=$(print -P %F{207}%%E%F{213} elapsed'  '%F{207}%%P%F{213} cpu'  '%F{207}%%MMB%F{213} memory%f)

# TODO: add bell and toast after long command completes:
# https://gist.github.com/jpouellet/5278239

SPACESHIP_PROMPT_ORDER=(${SPACESHIP_PROMPT_ORDER#hg})
SPACESHIP_PROMPT_ORDER=(${SPACESHIP_PROMPT_ORDER#docker})
unset RPROMPT

unalias run-help 2>/dev/null
autoload -U run-help
autoload run-help-sudo
autoload run-help-git

zstyle ':completion:*' completer _complete _correct _approximate

autoload -U select-word-style
select-word-style bash

__backward_argument() {
	select-word-style shell
	zle backward-word
	select-word-style bash
}

__forward_argument() {
	select-word-style shell
	zle forward-word
	select-word-style bash
}

zle -N __backward_argument __backward_argument
zle -N __forward_argument __forward_argument

backward-kill-dir () {
	local WORDCHARS=${WORDCHARS/\/}
	zle backward-kill-word
}
zle -N backward-kill-dir
bindkey '^w' backward-kill-dir

bindkey ';5D' __backward_argument
bindkey ';5C' __forward_argument

bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

bindkey '^[[3;5~' kill-word

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
	source /etc/profile.d/vte.sh
fi

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

bindkey "^[OA" "history-substring-search-up"
bindkey "^[[A" "history-substring-search-up"
bindkey "^[OB" "history-substring-search-down"
bindkey "^[[B" "history-substring-search-down"

#[[ -n "$key[Up]"   ]] && bindkey -- "$key[Up]"   up-line-or-beginning-search
#[[ -n "$key[Down]" ]] && bindkey -- "$key[Down]" down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search

bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char-or-list

##############################################################################
# History Configuration
##############################################################################
HISTSIZE=5000               #How many lines of history to keep in memory
HISTFILE=~/.zsh_history     #Where to save history to disk
SAVEHIST=5000               #Number of history entries to save to disk
#HISTDUP=erase               #Erase duplicates in the history file
#setopt    appendhistory     #Append history to the history file (no overwriting)
#setopt    sharehistory      #Share history across terminals
#setopt    incappendhistory  #Immediately append to the history file, not just when a term is killed

setopt append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_dups # ignore duplication command history list
setopt hist_ignore_space
setopt hist_verify
setopt inc_append_history
setopt share_history # share command history data


alias l="ls -lhF --color"
alias la="ls -lhaF --color"
alias ls="ls -hF --color"
alias df="df --exclude-type=devtmpfs --exclude-type=tmpfs"
alias xo="xdg-open"
function mkcd () {
	mkdir -p "$1" && cd "$1"
}

function gogo () {
	cd "$GOPATH/src/$1"
}

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
	printf "Install? [y/N]: "
	if read -q; then
		echo; zplug install
	fi
fi

zplug load
