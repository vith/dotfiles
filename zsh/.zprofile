[ -f /etc/profile.d/apps-bin-path.sh ] && source /etc/profile.d/apps-bin-path.sh

# Some distros/desktop environments don't implement environment.d support.
# Others do but overwrite the env vars *after* the user's changes.
for x in ~/.config/environment.d/*.conf; do
    source <(sed -e 's/^/export /' < $x)
done

# fix vscode stealing the default application association for directories
if [ "$(xdg-mime query default inode/directory)" = "visual-studio-code.desktop" ]; then
    if [ -f "/usr/share/applications/org.gnome.Nautilus.desktop" ]; then
        xdg-mime default org.gnome.Nautilus.desktop inode/directory
    fi
fi
